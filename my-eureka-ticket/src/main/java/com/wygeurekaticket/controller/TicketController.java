package com.wygeurekaticket.controller;

import com.wygeurekaticket.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TicketController
 *
 * @author wyg
 * @since 2021年06月02日23:14
 */
@RestController
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @GetMapping("/getTicket")
    public String getTicket() {
        return ticketService.getTicket();
    }
}
