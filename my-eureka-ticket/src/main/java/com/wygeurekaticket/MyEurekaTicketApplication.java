package com.wygeurekaticket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyEurekaTicketApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyEurekaTicketApplication.class, args);
	}

}
