package com.wygeurekaticket.service;

/**
 * 电影票服务
 *
 * @author wyg
 * @since 2021年06月02日23:19
 */
public interface TicketService {
    String getTicket();
}
