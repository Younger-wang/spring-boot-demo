package com.wyg.consumer;

import com.wyg.consumer.service.MyUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MyDubboConsumerApplicationTests {

	@Autowired
	private MyUserService myUserService;

	@Test
	void contextLoads() {
		System.out.println(myUserService.takeMovie());
	}

}
