package com.wyg.consumer.service.impl;

import com.wyg.consumer.service.MyUserService;
import com.wyg.provider.service.TicketService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

/**
 * 观众service实现类
 *
 * @author wyg
 * @since 2021年05月27日22:31
 */
@Service
public class MyUserServiceImpl implements MyUserService {

    @Reference
    private TicketService ticketService;

    @Override
    public String takeMovie() {
        String ticket = ticketService.getTicket();
        return "买到票了：" + ticket;
    }
}
