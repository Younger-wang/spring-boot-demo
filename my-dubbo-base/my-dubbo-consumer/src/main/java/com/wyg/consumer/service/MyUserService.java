package com.wyg.consumer.service;

/**
 * 观众service
 *
 * @author wyg
 * @since 2021年05月27日22:30
 */
public interface MyUserService {
    String takeMovie();
}
