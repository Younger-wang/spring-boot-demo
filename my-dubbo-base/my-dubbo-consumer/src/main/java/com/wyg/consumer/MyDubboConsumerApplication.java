package com.wyg.consumer;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 1、引入dubbo和zkclient依赖
 * 2、配置dubbo的注册中心地址
 * 3、引用服务@Reference
 */
@EnableDubbo
@SpringBootApplication
public class MyDubboConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyDubboConsumerApplication.class, args);
	}

}
