package com.wyg.provider.service;

/**
 * 电影票Service，映射服务提供者
 * 这里的包名要和提供者的服务完全一致
 *
 * @author wyg
 * @since 2021年05月27日22:34
 */
public interface TicketService {
    String getTicket();
}
