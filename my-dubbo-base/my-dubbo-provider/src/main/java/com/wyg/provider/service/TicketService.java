package com.wyg.provider.service;

/**
 * 电影票Service
 *
 * @author wyg
 * @since 2021年05月27日22:34
 */
public interface TicketService {
    String getTicket();
}
