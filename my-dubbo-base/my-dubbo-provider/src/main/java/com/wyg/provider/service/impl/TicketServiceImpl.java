package com.wyg.provider.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.wyg.provider.service.TicketService;
import org.springframework.stereotype.Component;

/**
 * 电影票Service实现类
 *
 * @author wyg
 * @since 2021年05月27日22:34
 */
@Service
@Component
public class TicketServiceImpl implements TicketService {
    @Override
    public String getTicket() {
        return "刺杀小说家";
    }
}
