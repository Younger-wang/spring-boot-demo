package com.wyg.eureka.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 注册中心
 * 1、配置eureka信息
 * 2、@EnableEurekaServer 启用EurekaServer
 */
@EnableEurekaServer
@SpringBootApplication
public class MyEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyEurekaServerApplication.class, args);
	}

}
