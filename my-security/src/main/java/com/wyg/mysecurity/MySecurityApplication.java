package com.wyg.mysecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 1、引入SpringSecurity依赖
 * 2、编写SpringSecurity的配置类
 * 		需要开启@EnableWebSecurity 且 继承WebSecurityConfigurerAdapter
 * 3、控制请求的访问权限
 */
@SpringBootApplication
public class MySecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(MySecurityApplication.class, args);
	}

}
