package com.wyg.mysecurity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类说明
 *
 * @author wyg
 * @since 2021年05月25日23:14
 */
@RestController
@RequestMapping("/article")
public class ArticleController {

    @GetMapping("/v1/hello")
    public String v1Hello() {
        return "这是v1的请求";
    }

    @GetMapping("/v2/hello")
    public String v2Hello() {
        return "这是v2的请求";
    }

    @GetMapping("v3/hello")
    public String v3Hello() {
        return "这是v3的请求";
    }

}
