package com.wyg.mysecurity.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * security配置类
 *
 * @author wyg
 * @since 2021年05月25日23:33
 */
@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * 定制授权规则
     *
     * @param http http
     * @throws Exception Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 定制请求的授权规则
//        http.authorizeRequests(authorize ->
//                authorize
//                .antMatchers("/").permitAll()
//                .antMatchers("/login.html").permitAll()
//                .antMatchers("/article/v1/**").hasRole("v1")
//                .antMatchers("/article/v2/**").hasRole("v2")
//                .antMatchers("/article/v3/**").hasRole("v3")
//                .anyRequest().authenticated()
//                )
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/login.html").permitAll()
                .antMatchers("/article/v1/**").hasRole("v1")
                .antMatchers("/article/v2/**").hasRole("v2")
                .antMatchers("/article/v3/**").hasRole("v3")
                .anyRequest().authenticated()
                .and()


                // 开启自动配置的登录功能，效果，如果没有权限，就会跳转到登录页面
                // 1、/login来到登录页
                // 2、重定向到/login？error表示登录失效
                // 3、更多相信规定
                .formLogin()
//                .loginPage("/login").permitAll()
                .and()
                .logout().logoutSuccessUrl("/").permitAll()
                .and()
                // 开启记住我功能
                // 登录成功以后，将cookie发给浏览器保存，以后访问带上这个cookie，只要通过检查就可以免登陆
                // 点击注销会删除cookie
                .rememberMe();
    }

    /**
     * 定制认证规则
     * @param auth auth
     * @throws Exception Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
                .withUser("zhangsan").password(new BCryptPasswordEncoder().encode("123456")).roles("v1")
                .and()
                .withUser("lisi").password(new BCryptPasswordEncoder().encode("123456")).roles("v2")
                .and()
                .withUser("wangwu").password(new BCryptPasswordEncoder().encode("123456")).roles("v3");
    }
}
