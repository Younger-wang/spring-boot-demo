package com.wyg.eurekaperson.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.QueryParam;

/**
 * 类说明
 *
 * @author wyg
 * @since 2021年06月03日0:46
 */
@RestController
@RequestMapping("/movie")
public class MovieController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/buy")
    public String buyTicket(@QueryParam("username") String username) {
        String ticket = restTemplate.getForObject("http://PROVIDER-TICKET/ticket/getTicket", String.class);
        return username + "购买了" + ticket;
    }
}
