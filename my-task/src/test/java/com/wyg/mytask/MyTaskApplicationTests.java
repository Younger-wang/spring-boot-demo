package com.wyg.mytask;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.File;
import java.io.UnsupportedEncodingException;

@SpringBootTest
class MyTaskApplicationTests {

	@Autowired
	private JavaMailSender javaMailSender;

	@Test
	void sendMail() {
		// 发送一个简单的邮件
		SimpleMailMessage message = new SimpleMailMessage();
		// 邮件设置
		message.setSubject("通知，今晚开会");
		message.setText("今晚8.30开会");
		message.setTo("wangyg4546@sohu.com");
		message.setFrom("384219749@qq.com");
		javaMailSender.send(message);
	}

	@Test
	void sendMail02() throws MessagingException, UnsupportedEncodingException {
		// 创建一个复杂的消息邮件
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setSubject("通知，重要通知");
		helper.setText("<b style='color:red'><i>这是一条重要通知</i></b>", true);
		helper.setTo("wangyg4546@sohu.com");
		helper.setFrom("384219749@qq.com");
		String attachmentName = "在人间.mp3";
		// 解决附件名中文乱码
		String newName = MimeUtility.encodeWord(attachmentName, "utf-8", "B");
		helper.addAttachment(newName, new File("D:\\LocalFile\\在人间.mp3"));
		javaMailSender.send(message);
	}
}
