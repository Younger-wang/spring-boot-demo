package com.wyg.mytask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 开启异步注解功能 @EnableAsync
 * 开启定时任务注解功能 @EnableScheduling
 */
@EnableScheduling // 开启定时任务注解功能
@EnableAsync // 开启异步注解功能
@SpringBootApplication
public class MyTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyTaskApplication.class, args);
	}

}
