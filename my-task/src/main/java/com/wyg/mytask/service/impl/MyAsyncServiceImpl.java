package com.wyg.mytask.service.impl;

import com.wyg.mytask.service.MyAsyncService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class MyAsyncServiceImpl implements MyAsyncService {

    @Async
    @Override
    public void sayHello() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        System.out.println("任务处理完成");
    }
}
