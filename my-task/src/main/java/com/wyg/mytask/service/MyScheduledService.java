package com.wyg.mytask.service;

/**
 * 接口说明：定时任务service
 *
 * @author wyg
 * @since 2021年05月20日8:25
 */
public interface MyScheduledService {
    void scheduledSayHello();
}
