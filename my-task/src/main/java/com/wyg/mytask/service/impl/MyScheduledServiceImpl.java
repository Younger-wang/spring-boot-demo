package com.wyg.mytask.service.impl;

import com.wyg.mytask.service.MyScheduledService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * 类说明：定时任务实现类
 *
 * @author wyg
 * @since 2021年05月20日8:27
 */
@Service
public class MyScheduledServiceImpl implements MyScheduledService {

    @Override
//    @Scheduled(cron = "0 * * * * ?") // 每分钟的0秒执行一次
//    @Scheduled(cron = "0/10 * * * * ?") // 每天启动之后从0秒开始每隔10秒执行一次
//    @Scheduled(cron = "0/10 * * * * 0-7") // 每天启动之后从0秒开始每隔10秒执行一次
//    @Scheduled(cron = "0 0/5 14,18 * ?") // 每天的14：00和18:00开始每5分钟执行一次
//    @Scheduled(cron = "0 15 10 ? * 1-6") // 每月的周一到周六的10:15执行一次
//    @Scheduled(cron = "0 0 2 ? * 6L") // 每月最后一个周六的2:00执行一次
//    @Scheduled(cron = "0 0 2 WL * ?") // 每月最后一个工作日的2:00执行一次
    @Scheduled(cron = "0 0 2-4 ? * 4#2") // 每月第二个星期四的2:00-4:00整点执行一次
    public void scheduledSayHello() {
        System.out.println("scheduled Hello..." + LocalDateTime.now());
    }
}
