package com.wyg.mytask.service;

public interface MyAsyncService {

    // 测试异步任务，需要在启动类中开启@EnableAsync注解

    void sayHello() throws InterruptedException;
}
