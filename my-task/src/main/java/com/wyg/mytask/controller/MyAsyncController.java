package com.wyg.mytask.controller;

import com.wyg.mytask.service.MyAsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyAsyncController {

    @Autowired
    private MyAsyncService myAsyncService;

    @GetMapping("/hello")
    public String hello() throws InterruptedException {
        myAsyncService.sayHello();
        return "success";
    }
}
