package com.wyg.myelasticsearch;

import com.wyg.myelasticsearch.beans.Person;
import com.wyg.myelasticsearch.repository.PersonRepository;
import io.searchbox.client.JestClient;
import io.searchbox.core.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MyElasticsearchApplicationTests {

	@Autowired
	private JestClient jestClient;

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;

	@Test
	public void putESTest() {
		Person person = new Person();
		person.setName("lisi");
		person.setAddr("高新区");
		person.setAge(33);
		Index index = new Index.Builder(person).index("wyg-person").type("person").id("1").build();
		try {
			DocumentResult result = jestClient.execute(index);
			System.out.println(result.getJsonString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void delESTest() {
		Delete delete = new Delete.Builder("_9NBeXcBFEWBykCqLSPj").index("wyg-person").type("person").build();
		try {
			DocumentResult result = jestClient.execute(delete);
			System.out.println(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void searchESTest() {
		String query = "";
		Search search = new Search.Builder(query).addIndex("wyg-person").addType("person").build();
		try {
			SearchResult result = jestClient.execute(search);
			System.out.println(result.getJsonString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDataRepository() {
		Person person = new Person();
		person.setId(1);
		person.setName("zhangsan1");
		person.setAddr("高新区");
		person.setAge(22);
		Person index = personRepository.index(person);
		System.out.println(index);
	}

	@Test
	public void testDataLike() {
		List<Person> personList = personRepository.findByNameLike("san");
		System.out.println(personList);
	}

	@Test
	public void testDataTemplate() {
		Pageable pageable = new PageRequest(0, 20, new Sort(Sort.Direction.DESC, "name"));
		SearchQuery searchQuery = new NativeSearchQueryBuilder()
//				.withQuery(QueryBuilders.queryStringQuery(""))
				.withPageable(pageable)
			.build();
		AggregatedPage<Person> people = elasticsearchTemplate.queryForPage(searchQuery, Person.class);
		System.out.println(people.getContent());
	}
}
