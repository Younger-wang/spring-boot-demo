package com.wyg.myelasticsearch.beans;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "wyg-person", type = "person")
@Data
public class Person {
    private Integer id;
    private String name;
    private Integer age;
    private String addr;
}
