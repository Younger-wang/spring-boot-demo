package com.wyg.myelasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 使用1.5.12版本
 * springboot默认使用两种技术来和ES交互
 * 1、Jest（默认不生效）
 * 	需要导入Jest的工具包：（io.searchbox.client.JestClient）
 * 2、SpringData ElasticSearch【ES版本可能不合适】
 * 	版本适配说明：https://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/#preface.versions
 * 	如果版本不适配：版本2.4.6
 * 		1）升级springboot版本
 * 		2）安装对应版本的ES
 * 	1）Client节点信息 clusterNodes，clusterName
 * 	2）ElasticSearchTemplate操作ES
 * 	3）编写ElasticSearchRepository子接口来操作ES
 * 		用法：https://github.com/spring-projects/spring-data-elasticsearch
 */
@SpringBootApplication
public class MyElasticsearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyElasticsearchApplication.class, args);
	}

}
