package com.wyg.myelasticsearch.repository;

import com.wyg.myelasticsearch.beans.Person;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PersonRepository extends ElasticsearchRepository<Person, Integer> {
    // 参照 https://docs.spring.io/spring-data/elasticsearch/docs/3.2.13.RELEASE/reference/html/#repositories.query-methods
    List<Person> findByNameLike(String personName);
}
